var gulp = require('gulp'),
gulpsync = require('gulp-sync')(gulp),
uglify = require('gulp-uglify'),
babel = require('gulp-babel'),
clean = require('gulp-clean'),
zip = require('gulp-zip'),
jsonfile = require('jsonfile');

const package = require('./package.json');
const DEST_TMP = './dist/tmp/';

gulp.task('before', function () {
return gulp.src('./dist')
    .pipe(clean());
});

gulp.task('build', function() {
return gulp.src('src/**/*.js')
    .pipe(babel({
        presets: ["es2015"]
    }))
    .pipe(uglify({mangle:{toplevel:true}}))
    .pipe(gulp.dest(DEST_TMP + 'bin/'));
});

gulp.task('config', function () {

const newPackage = package;
delete newPackage.devDependencies;
delete newPackage.scripts.dev;
delete newPackage.scripts.build;
delete newPackage.scripts.devSendStatus;
const filepath = DEST_TMP + 'package.json';
jsonfile.writeFileSync(filepath, newPackage, {spaces: 4, EOL: '\r\n'});

return gulp.src(['server.config.js', 'pm2.json'])
    .pipe(gulp.dest(DEST_TMP));
});

gulp.task('zip', function () {
const destFilename = 'server_ver_' + package.version + '.zip';
return gulp.src(DEST_TMP + '**')
    .pipe(zip(destFilename))
    .pipe(gulp.dest('dist'))
});

gulp.task('after', function () {
return gulp.src(DEST_TMP)
    .pipe(clean());
});

gulp.task('default', gulpsync.sync([
'before',
'build',
'config',
'zip',
'after'
]));