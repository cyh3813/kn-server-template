import ulog from  './ulog';
import express from 'express';
import bodyParser from 'body-parser';
import cfg from '../server.config';

const TAG = '[SERVER]';

initLog();
startExpressServer(cfg.port);

function initLog() {
    ulog.init('change_template_name');
    global.ulog = ulog;
    ulog.info(TAG, 'change template name');
}

function startExpressServer(port) {
    const app = express();
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

    app.get('/', (req, res, next) => {
        res.json({status: 'ok', timestamp: new Date().getTime()});
    });
    app.get((error, req, res, next) => {
        res.status(500).json({status: 'ok', timestamp: new Date().getTime(), message: error.message});
    });
    
    app.listen(port || 3000, () => {
        ulog.info(TAG, `[kn-server-template] listening on port : ${port || 3000}`);
    });
}