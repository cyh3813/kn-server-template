# README #

babel, gulp build, jasmine test, winston log, pm2 run supported express server template

### 1. test ###
- yarn test

### 2. dev run ###
- yarn dev

### 3. build ###
- yarn build
- (create zip file to dist/)

### 4. product run  ###
- unzip dist/server.zip after build
- yarn start 
- (pm2 start pm2.json)

### 5. product stop  ###
- pm2 stop pm2.json
- (pm2 stop [pm2 id])
